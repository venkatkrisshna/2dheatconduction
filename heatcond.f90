! 2D Heat Conduction Solver
! Venkata Krisshna
! EMEC 592 - HPC & Multiphase Flows
! This program solves a 2D steady state heat conduction equation

program improvedheatconduction
  
  implicit none
  include 'mpif.h'
  include 'silo_f9x.inc'
  integer, parameter :: wp=kind(1.0d0)
  integer ierror, rank, numproc, status(MPI_STATUS_SIZE), dimx, dimy, dimeqx, dimeqy, local_dimx, local_dimy, procx, procy
  integer local_imin, local_imax, local_jmin, local_jmax, imax, imin, jmax, jmin, xmax, xmin, ymax, ymin
  integer icount, jcount, proccount, iter, up, down, left, right, gszlr, gszud, loctempsz, dims(2)
  integer old_comm, new_comm, ndims, reorder, dim_size(2), coord(2), sinfo, masteri, masterj, temprcx, temprcy
  real(WP) lengthx, lengthy, deltax, deltay, sij, timj, tipj, tijm, tijp, interior_tij
  real(WP) leftt, rightt, bott, topt, lefth, righth, both, toph, lefttamb, righttamb, bottamb, toptamb
  real(WP) h, biot, tamb, k, rightb, leftb, topb, botb, tolsum, tol, tolmax, tolreq
  real(WP), dimension(:, :), allocatable :: master_temp, local_temp, local_temp0, local_temprc
  real(WP), dimension(:, :), allocatable :: sendrt, sendlt, sendup, senddn, recvrt, recvlt, recvup, recvdn
  real(WP), dimension(:), allocatable :: xmesh, ymesh, local_xmesh, local_ymesh
  integer, dimension(:, :), allocatable :: local_info
  logical periods(0:1)
  integer :: dbfile
  character(len=50) :: siloname, Left_Boundary, Right_Boundary, Bottom_Boundary, Top_Boundary, dirname
  
  call MPI_INIT(ierror)
  call MPI_COMM_RANK(mpi_comm_world,rank,ierror)
  call MPI_COMM_SIZE(mpi_comm_world,numproc,ierror)

  ! ===============================================
  ! Defining problem statement and input parameters
  ! ===============================================

  ! Number of processors
  procx   = 2             ! Processors in x
  procy   = 2             ! Processors in y

  ! Domain geometry
  lengthx = 1             ! Length of the wall cross section in x
  lengthy = 1             ! Length of the wall cross section in y
  dimx    = 4             ! Number of nodes in x direction
  dimy    = 4             ! Number of nodes in y direction

  ! Source term
  sij     = 0

  ! Boundary conditions
  Left_Boundary    = 'neumann'
  Bottom_Boundary  = 'neumann'
  Top_Boundary     = 'neumann'
  Right_Boundary   = 'dirichlet'
  rightt           = 500
  ! Left_Boundary    = 'dirichlet'
  ! leftt            = 505
  ! Right_Boundary   = 'dirichlet'
  ! rightt           = 0
  ! ! Right_Boundary   = 'robin'
  ! ! righth           = 500
  ! ! righttamb        = 200
  ! Bottom_Boundary  = 'dirichlet'
  ! bott             = 100                      
  ! Top_Boundary     = 'dirichlet'
  ! topt             = 200
  ! ! Top_Boundary     = 'neumann'
  ! ! topq             = 0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!! USE THIS INTO CODE.
  
  ! ---------------------------------------------------------------------
  ! Use the following format
  ! <Side>_Boundary = 'dirichlet/robin/neumann'
  ! leftt           = Temperature for dirichlet T
  ! lefth           = Convective heat transfer coefficient for convection
  ! ltamb           = Ambient temperature for convection
  ! ---------------------------------------------------------------------
  
  ! Material Data
  k = 0.15          ! Thermal conductivity

  ! declaring computational parameters
  tolreq  = 1e-1          ! required tolerance
  deltax  = lengthx/dimx  ! Distance between two adjacent nodes in x
  deltay  = lengthy/dimy  ! Distance between two adjacent nodes in y
  iter    = 0             ! Number of iterations
  tol     = 99            ! Tolerance
  tolmax  = 99            ! Tolerance

  if(numproc.ne.procx*procy)then
     if(rank.eq.0) print*,'INCORRECT NUMBER OF PROCESSORS SPECIFIED!'
  else
     old_comm    = MPI_COMM_WORLD
     ndims       = 2          ! 2D matrx/grid
     dim_size(1) = procy      ! rows
     dim_size(2) = procx      ! columns
     periods(0)  = .false.    ! row-periodic (each column wraps around)
     periods(1)  = .false.    ! column-nonperiodic
     reorder     = 0

     ! Creating Cartesian grid for parallel computing
     call MPI_CART_CREATE(old_comm, ndims, dim_size, periods, reorder, new_comm, ierror)
     call MPI_CART_COORDS(new_comm, rank, ndims, coord, ierror)
     call MPI_CART_SHIFT(new_comm, 0, 1, up, down, ierror)
     call MPI_CART_SHIFT(new_comm, 1, 1, left, right, ierror)
     ! print*,'My rank is',rank,'and my coords are',coord(1),coord(2)
     ! print*,'Rank',rank,'neighbors are left:',left,'Right:',right,'Up:',up,'Down:',down
    
     ! Splitting the job
     dimeqx = dimx - mod(dimx,procx)
     dimeqy = dimy - mod(dimy,procy)
     local_dimx  = dimeqx/procx
     local_dimy  = dimeqy/procy
     if(coord(2).eq.procx-1) local_dimx = local_dimx + mod(dimx,procx)
     if(coord(1).eq.procy-1) local_dimy = local_dimy + mod(dimy,procy)

     local_imin = 0                                         ! Lower bound in x for each processor
     if(procx.eq.1)then
        local_imax = dimx - 1                               ! Upper bound in x for single processor
     else if(coord(2).eq.0.or.coord(2).eq.(procx-1))then
        local_imax = local_dimx                             ! Upper bound in x for boundary processors 
     else
        local_imax = local_dimx + 1                         ! Upper bound in x for interior processors
     end if

     local_jmin = 0                                         ! Lower bound in y for each processor
     if(procy.eq.1)then
        local_jmax = dimy - 1                               ! Upper bound in y for single processor
     else if(coord(1).eq.0.or.coord(1).eq.(procy-1))then
        local_jmax = local_dimy                             ! Upper bound in y for boundary processors 
     else
        local_jmax = local_dimy + 1                         ! Upper bound in y for interior processors
     end if
     ! print*,'rank -',rank,'/ imin -',local_imin,'/ imax -',local_imax,'/ jmin -',local_jmin,'/ jmax -',local_jmax
     ! print*,'rank -',rank,'/ dimx -',local_dimx,'/ dimy -',local_dimy

     ! Allocating arrays
     allocate(local_temp  (local_imin:local_imax,local_jmin:local_jmax))
     allocate(local_temp0 (local_imin:local_imax,local_jmin:local_jmax))
     local_temp  = 10
     local_temp0 = 0
     allocate(xmesh(0:dimx-1))
     allocate(ymesh(0:dimy-1))
     allocate(sendrt(1,0:local_jmax))
     allocate(sendlt(1,0:local_jmax))
     allocate(recvrt(1,0:local_jmax))
     allocate(recvlt(1,0:local_jmax))
     allocate(sendup(0:local_imax,1))
     allocate(senddn(0:local_imax,1))
     allocate(recvup(0:local_imax,1))
     allocate(recvdn(0:local_imax,1))
     allocate(local_info(0:12,1))
     local_info = 0
     gszlr     = int(size(sendrt))
     gszud     = int(size(sendup))
     sinfo     = int(size(local_info))

     ! ===============================
     ! Begin temperature calculations
     ! ===============================
     
     do iter = 1,1
     ! do while(tolmax>tolreq)                                           ! Do till tolerance condition is satisfied
     !    iter = iter + 1                                              ! Increments on iter

        ! Update boundaries for each processor
        ! ------------------------------------

        if(coord(1).eq.0)then                                 ! Top wall
        ! if(coord(1).eq.procy-1)then                           ! Bottom wall
           if(Top_Boundary.eq.'dirichlet')then
              local_temp(:,local_jmin) = topt
           else if(Top_Boundary.eq.'neumann')then
              local_temp(:,local_jmin) = local_temp(:,local_jmin+1)
           else if(Top_Boundary.eq.'robin')then
              do icount = local_imin,local_imax
                 local_temp(icount,local_jmin) = ((toph*deltay/k)*toptamb + &
                      local_temp(icount,local_jmin+1))/(1 + (toph*deltay/k))
              end do
           end if
        end if
        if(coord(1).eq.procy-1)then                           ! Bottom wall
        ! if(coord(1).eq.0)then                                 ! Top wall
           if(Bottom_Boundary.eq.'dirichlet')then
              local_temp(:,local_jmax) = bott
           else if(Bottom_Boundary.eq.'neumann')then
              local_temp(:,local_jmax) = local_temp(:,local_jmax-1)
           else if(Bottom_Boundary.eq.'robin')then
              do icount = local_imin,local_imax
                 local_temp(icount,local_jmax) = ((both*deltay/k)*bottamb + &
                      local_temp(icount,local_jmax-1))/(1 + (both*deltay/k))
              end do
           end if
        end if
        if(coord(2).eq.0)then                                 ! Left wall
           if(Left_Boundary.eq.'dirichlet')then
              local_temp(local_imin,:) = leftt
           else if(Left_Boundary.eq.'neumann')then
              local_temp(local_imin,:) = local_temp(local_imin+1,:)
           else if(Left_Boundary.eq.'robin')then
              do jcount = local_jmin,local_jmax
                 local_temp(local_imin,jcount) = ((lefth*deltax/k)*lefttamb + &
                      local_temp(local_imin+1,jcount))/(1 + (lefth*deltax/k))
              end do
           end if
        end if
        if(coord(2).eq.procx-1)then                           ! Right wall
           if(Right_Boundary.eq.'dirichlet')then
              local_temp(local_imax,:) = rightt
           else if(Right_Boundary.eq.'neumann')then
              local_temp(local_imax,:) = local_temp(local_imax-1,:)
           else if(Right_Boundary.eq.'robin')then
              do jcount = local_jmin,local_jmax
                 local_temp(local_imax,jcount) = ((righth*deltax/k)*righttamb + &
                      local_temp(local_imax-1,jcount))/(1 + (righth*deltax/k))
              end do
           end if
        end if

        ! Ghost cell data exchange between processors
        ! -------------------------------------------
        
        if(right.ne.-2)then
           sendrt(1,0:local_jmax) = local_temp(local_imax-1,:)
           call MPI_SENDRECV(sendrt, gszlr, mpi_double_precision, right, 1, &
                recvrt, gszlr, mpi_double_precision, right, 1, old_comm, status, ierror)
           local_temp(local_imax,:) = recvrt(1,0:local_jmax)
        end if
        if(left.ne.-2)then
           sendlt(1,0:local_jmax) = local_temp(local_imin+1,:)
           call MPI_SENDRECV(sendlt, gszlr, mpi_double_precision, left, 1, &
                recvlt, gszlr, mpi_double_precision, left, 1, old_comm, status, ierror)
           local_temp(local_imin,:) = recvlt(1,0:local_jmax)
        end if
        if(up.ne.-2)then
           sendup(0:local_imax,1) = local_temp(:,local_jmin+1)
           call MPI_SENDRECV(sendup, gszud, mpi_double_precision, up, 1, &
                recvup, gszud, mpi_double_precision, up, 1, old_comm, status, ierror)
           local_temp(:,local_jmin) = recvup(0:local_imax,1)
        end if
        if(down.ne.-2)then
           senddn(0:local_imax,1) = local_temp(:,local_jmax-1)
           call MPI_SENDRECV(senddn, gszlr, mpi_double_precision, down, 1, &
                recvdn, gszud, mpi_double_precision, down, 1, old_comm, status, ierror)
           local_temp(:,local_jmax) = recvdn(0:local_imax,1)
        end if

        ! Sweeping through interior nodes
        ! -------------------------------

        do icount = local_imin+1,local_imax-1
           do jcount = local_jmin+1,local_jmax-1
              timj = local_temp(icount-1,jcount)
              tipj = local_temp(icount+1,jcount)
              tijm = local_temp(icount,jcount-1)
              tijp = local_temp(icount,jcount+1)
              call itij(deltax, deltay, sij, timj, tipj, tijm, tijp, interior_tij)
              local_temp(icount,jcount) = interior_tij
           end do
        end do

        ! Computing tolerance
        ! -------------------

        tolsum = 0
        do icount = local_imin,local_imax
           do jcount = local_jmin,local_jmax
              tolsum = tolsum + ((local_temp(icount,jcount))-(local_temp0(icount,jcount)))**2
              tol = sqrt(tolsum)/maxval(local_temp)
           end do
        end do
        call MPI_ALLREDUCE(tol, tolmax, sizeof(tol), mpi_double_precision, mpi_max, mpi_comm_world, ierror)
        ! Updating temp0 and iter values
        local_temp0 = local_temp
     end do  ! Iterations / Tolerance do loop
     
     ! ===============================
     ! End of temperature calculations
     ! ===============================
     
     if(rank.eq.1)then
        print*,'Tolerance criterion is satisfied after',iter,'iterations.'
     end if
     
     ! ==========================
     ! Building Master Temp array
     ! ==========================
     
     ! Sending local_temp arrays to root
     if(numproc.gt.1)then
        if(rank.ne.0)then
           local_info(0,1)  = right
           local_info(1,1)  = left
           local_info(2,1)  = up
           local_info(3,1)  = down
           local_info(4,1)  = coord(1)
           local_info(5,1)  = coord(2)
           local_info(6,1)  = local_imin
           local_info(7,1)  = local_imax
           local_info(8,1)  = local_jmin
           local_info(9,1)  = local_jmax
           local_info(10,1) = local_dimx
           local_info(11,1) = local_dimy
           loctempsz = int(size(local_temp,1)*size(local_temp,2))
           local_info(12,1) = loctempsz
           call MPI_SEND(local_info, sinfo, mpi_double_precision, 0, 1, mpi_comm_world, ierror)
           call MPI_SEND(local_temp, loctempsz, mpi_double_precision, 0, 2, mpi_comm_world, ierror)
        end if
     end if
     
     if(rank.eq.0)then
        allocate(master_temp(0:dimx-1,0:dimy-1))             ! Allocating Master Temp array
        master_temp = 0                                      ! Initializing Master Temp array
        if(numproc.gt.1)then
           ! Populating Master with data from proc 0
           imin = local_imin
           imax = local_imax
           jmin = local_jmin
           jmax = local_jmax
           call popmaster(right, left, up, down, imax, imin, jmin, jmax)
           masteri = 0
              do icount = imin,imax
              masterj = 0
                 do jcount = jmin,jmax
                 master_temp(masteri,masterj) = local_temp(icount,jcount)
                 masterj = masterj + 1
              end do
              masteri = masteri + 1
           end do
           do proccount = 1,numproc-1
              ! Receiving local_temp from other procs
              call MPI_RECV(local_info, sinfo, mpi_double_precision, proccount, 1, mpi_comm_world, status, ierror)
              loctempsz = local_info(12,1)
              temprcx = local_info(7,1)
              temprcy = local_info(9,1)
              allocate(local_temprc(0:temprcx,0:temprcy))
              local_temprc = 0
              call MPI_RECV(local_temprc, loctempsz, mpi_double_precision, proccount, 2, mpi_comm_world, status, ierror)
              ! Populating Master with data from other procs
              right = local_info(0,1)
              left  = local_info(1,1)
              up    = local_info(2,1)
              down  = local_info(3,1)
              imax  = local_info(7,1)
              imin  = local_info(6,1)
              jmin  = local_info(8,1)
              jmax  = local_info(9,1)
              call popmaster(right, left, up, down, imax, imin, jmin, jmax)
              masteri = local_info(5,1)*local_dimx
              do icount = imin,imax
                 masterj = local_info(4,1)*local_dimy
                 do jcount = jmin,jmax
                    master_temp(masteri,masterj) = local_temprc(icount,jcount)
                    masterj = masterj + 1
                 end do
                 masteri = masteri + 1
              end do
              deallocate(local_temprc)
           end do
        else
           master_temp = local_temp
        end if
        print*,'PROGRAM COMPLETED!'
        ! Writing Master to CSV file
        ! --------------------------
        ! open(unit=21, file="master_temp.csv", action="write", status="replace")
        ! print*,'MASTER!'
        do jcount=0,dimy-1
           print*,master_temp(:,jcount)
        ! !    write(21,*) (master_temp(:,jcount))
        end do
        ! close(unit=21)
     end if
     
     ! ==================
     ! Write data to SILO
     ! ==================
     
     ! Make Visit directory
     if (rank.eq.0) call execute_command_line('mkdir -p Visit')
     
     ! Create the silo database
     ! ------------------------
     write(siloname,'(A,I1,A)') 'Visit/step',1,'.silo'
     if (rank.eq.0) then
        ierror = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database", 13, DB_HDF5, dbfile)
        if(dbfile.eq.-1) print *,'Could not create Silo file!'
        ierror = dbclose(dbfile)
     end if
     call MPI_BARRIER(MPI_COMM_WORLD,ierror)
     
     ! Each Proc writes their data
     ! ---------------------------
     
     ! Loop over procs and each proc writes their portion of the mesh and data
     do proccount=0,numproc
        if (rank.eq.proccount) then ! This proc gets to write
           ! Open silo file 
           ierror = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
           ! Create directory inside silo file for this proc
           write(dirname,'(I5.5)') rank
           ierror = dbmkdir(dbfile,dirname,5,ierror)
           ierror = dbsetdir(dbfile,dirname,5)
           xmin = coord(2)*dimeqx/procx
           xmax = xmin + local_dimx - 1
           ymin = coord(1)*dimeqy/procy
           ymax = ymin + local_dimy - 1
           allocate(local_xmesh(xmin:xmax))
           allocate(local_ymesh(ymin:ymax))
           do icount = xmin,xmax
              local_xmesh(icount) = icount/lengthx
           end do
           do jcount = ymin,ymax
              local_ymesh(jcount) = jcount/lengthy
           end do
           dims(1) = local_dimx
           dims(2) = local_dimy
           ! Write quad-mesh for this proc
           ierror = dbputqm(dbfile, 'Mesh', 4, 'xc', 2, 'yc', 2, 'zc', 2, &
                local_xmesh, local_ymesh, DB_F77NULL, dims, 2, DB_DOUBLE, &
                DB_COLLINEAR, DB_F77NULL, ierror)
           ! Write data for this proc
           ierror = dbputqv1(dbfile, 'Temp', 4, 'Mesh', 4, local_temp, &
                (/local_dimx,local_dimy/), 2, DB_F77NULL, 0, DB_DOUBLE, &
                DB_NODECENT,DB_F77NULL, ierror)
           ! Close silo file
           ierror = dbclose(dbfile)
        end if
        ! Everyone waits until this proc is done
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)
     end do
     
     if (rank.eq.0) print*,'SILO FILE READY FOR VIEWING!'
     
     ! ! Declaring xmesh and ymesh points
     ! do icount = 0,dimx-1
     !    xmesh(icount) = ((icount+1)*lengthx)/dimx
     ! end do
     ! do jcount = 0,dimy-1
     !    ymesh(jcount) = ((jcount+1)*lengthy)/dimy
     ! end do
     ! dims(1) = dimx
     ! dims(2) = dimy
     ! write(siloname,'(A,I0.5,A)') 'master_temp.silo'
     ! ierror = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database", 13, DB_HDF5, dbfile)
     ! if(dbfile.eq.-1) print *,'Could not create Silo file!'
     ! ierror = dbclose(dbfile)
     ! ! Open silo file
     ! ierror = dbopen(siloname, len_trim(siloname), DB_HDF5, DB_APPEND, dbfile)
     ! ! Write quadmesh
     ! ierror = dbputqm(dbfile, 'Mesh', 4, 'xc', 2, 'yc', 2, 'zc', 2, &
     !      xmesh, ymesh, DB_F77NULL, dims, 2, DB_DOUBLE, DB_COLLINEAR, DB_F77NULL, ierror)
     ! ! Write data
     ! ierror = dbputqv1(dbfile, 'Temp', 4, 'Mesh', 4, master_temp, (/dimx,dimy/), &
     !      2, DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, ierror)
     ! ! Close silo file
     ! ierror = dbclose(dbfile)
     ! print*,'SILO FILE READY FOR VIEWING!'
     
  end if     ! For correct number of processors
  
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  call MPI_FINALIZE(ierror)

end program improvedheatconduction

! Subroutine to calculate interior local_temp
subroutine itij(dx, dy, sij, timj, tipj, tijm, tijp, interior_tij)
  integer, parameter :: wp=kind(1.0d0)
  real(WP), intent(in)  :: dx, dy, sij, timj, tipj, tijm, tijp
  real(WP), intent(out) :: interior_tij
  interior_tij = ((dx**2)*(dy**2)*sij - (timj + tipj)*(dy**2) - (tijm + tijp)*(dx**2))/(-2*((dx**2) + (dy**2)))
end subroutine itij

! Subroutine to calculate right wall local_temp
subroutine rtij(biot, timj, tamb, right_tij)
  integer, parameter :: wp=kind(1.0d0)
  real(WP), intent(in)  :: biot, timj, tamb
  real(WP), intent(out) :: right_tij
  right_tij = (biot*tamb + timj)/(1 + biot)
end subroutine rtij

! Subroutine to populate master_temp
subroutine popmaster(right, left, up, down, imax, imin, jmin, jmax)
  integer, parameter :: wp=kind(1.0d0)
  integer, intent(in)  :: right, left, up, down
  integer, intent(inout)  :: imax, imin, jmin, jmax
  if(right.ne.-2)then
     imax = imax-1
  end if
  if(left.ne.-2)then
     imin = imin+1
  end if
  if(up.ne.-2)then
     jmin = jmin+1
  end if
  if(down.ne.-2)then
     jmax = jmax-1
  end if
end subroutine popmaster
